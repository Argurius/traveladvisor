//
//  ContentView.swift
//  StopWatch
//
//  Created by Jakub Jurašek on 28/04/2021.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var stopWatch = StopWatch()
    
    var body: some View{
        ZStack {
            //background coolor
            Color(red: 51 / 255, green: 51 / 255, blue: 51 / 255)
                .edgesIgnoringSafeArea(.all)
            
            VStack {
                //customized time
                Text(String(stopWatch.formattedTime))
                    .font(.custom("Montserrat-Bold", size: 60))
                    .foregroundColor(Color.white)
                    .frame(width: UIScreen.main.bounds.size.width,
                            height: 350,
                            alignment: .center)
                
                Spacer()
                
                HStack {
                    
                    if stopWatch.currentMod == .running {
                        
                        Spacer()
                        
                    Button("Stop") {
                        self.stopWatch.pause()
                    }
                    .foregroundColor(.black)
                    .padding()
                    .background(Color.red)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                        
                        Spacer()
                        
                    Button("lap") {
                        self.stopWatch.addLap()
                        }
                        .foregroundColor(.black)
                        .padding()
                        .background(Color.orange)
                        .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                        
                        Spacer()
                    
                    }
                
                    if stopWatch.currentMod == .reset {
                    Button("Start") {
                        self.stopWatch.start()
                    }
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.green)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                }
                
                    if stopWatch.currentMod == .paused {
                        Spacer()
                        
                    Button("Reset") {
                        self.stopWatch.reset()
                    }
                    .foregroundColor(.black)
                    .padding()
                    .background(Color.red)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                        
                        Spacer()
                        
                        Button("Start") {
                            self.stopWatch.start()
                        }
                        .foregroundColor(.white)
                        .padding()
                        .background(Color.green)
                        .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                        
                        Spacer()
                }
            }
                
                VStack {
                    List {
                        ForEach (0..<stopWatch.arrayOfLaps.count, id: \.self) { index in
                            Text("Lap \(stopWatch.arrayOfLaps.count - index) :  \(stopWatch.arrayOfLaps[index], specifier: "%.2f")")
                        }
                    }.colorMultiply(Color(red: 128 / 255, green: 128 / 255, blue: 128 / 255))
                }
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
