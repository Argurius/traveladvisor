//
//  StopWatch.swift
//  StopWatch
//
//  Created by Jakub Jurašek on 28/04/2021.
//

import SwiftUI

enum stopWatchMod {
    case running, reset, paused
}

class StopWatch: ObservableObject {
    
    @Published var currentMod: stopWatchMod = .reset
    @Published var arrayOfLaps: [Double] = []
    @Published var formattedTime = "00:00.00"
    
    var timer = Timer()
    var seconds = 0.0
    var secondsElapsed = 0.0
    
    
    func start() {
        self.currentMod = .running
        timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { timer in
        self.seconds += 0.01
        self.formatTime()
        }
    }
    
    func addLap() {
        let totalTime = arrayOfLaps.reduce(0, +)
        let lapTime = self.seconds - totalTime
        //round to 2 digits
        let y = Double(round(100*lapTime)/100)
        
        if lapTime >= 0 {
                arrayOfLaps.insert(y, at: 0)
        } else {
            return
        }
    }
    
    func reset() {
        timer.invalidate()
        self.currentMod = .reset
        self.secondsElapsed = self.seconds
        self.seconds = 0.0
        self.formattedTime = "00:00.00"
        arrayOfLaps = []
    }
    
    func pause() {
        timer.invalidate()
        self.currentMod = .paused
    }
    //format time in stopwatch
    func formatTime() {
        let minutes: Int32 = Int32(self.seconds/60)
        let minutesString = (minutes < 10) ? "0\(minutes)" : "\(minutes)"
        let seconds: Int32 = Int32(self.seconds) - (minutes * 60)
        let secondsString = (seconds < 10) ? "0\(seconds)" : "\(seconds)"
        let milliseconds: Int32 = Int32(self.seconds.truncatingRemainder(dividingBy: 1) * 100)
        let millisecondsString = (milliseconds < 10) ? "0\(milliseconds)" : "\(milliseconds)"
        self.formattedTime = minutesString + ":" + secondsString + "." + millisecondsString
    }
}
