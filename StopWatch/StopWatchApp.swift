//
//  StopWatchApp.swift
//  StopWatch
//
//  Created by Jakub Jurašek on 28/04/2021.
//

import SwiftUI

@main
struct StopWatchApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
